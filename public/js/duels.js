$(function() {
    document.getElementById('buttonShrek').onclick = function () {
        let video = document.getElementById('shrekVideo');
        video.style.display = "";
        video.play();
        let winnerDisplay = setTimeout(function(){
            let result = document.getElementById('result').value;
            if(result === '1' || result === '2') {
                let winner = document.getElementById('winner');
                let player1 = document.getElementById(winner.value).value.toUpperCase();
                document.getElementById('result-frame').innerHTML += 'Le gagnant est ' + player1;
            } else {
                document.getElementById('result-frame').innerHTML += 'Égalité'
            }
                $('#result-frame').fadeIn('slow');
        }, 2300);
        //function that executes when the video ends
        video.addEventListener('ended',myHandler,false);
        function myHandler(e) {
            if(document.getElementById('result').value === 'draw'){
                window.location.reload();
            } else {
                let winner = document.getElementById('winner');
                let loser = document.getElementById('loser');
                let player1 = document.getElementById(winner.value).value.toUpperCase();
                let player2 = document.getElementById(loser.value).value.toUpperCase();
                let form = $('<form action="http://matioo.fr/scoreboard/addscore/" name="score" method="post" style="display:none;">' +
                    '<input type="text" name="player1" value="' + player1 + '" />' +
                    '<input type="text" name="player2" value="' + player2 + '" />' +
                    '<input type="text" name="key" value="team-matioo" />' +
                    '</form>');
                $('body').append(form);
                form.submit();
            }
        }
    }
});
