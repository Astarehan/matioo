CREATE TABLE `matioo`.`scoreboard` (
  `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `player` VARCHAR(100),	
  `score` INT,
  UNIQUE (ID),
  UNIQUE (player)
);