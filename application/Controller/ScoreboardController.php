<?php

namespace Mini\Controller;

use Mini\Model\Scoreboard;

class ScoreboardController
{
    public function index() {
        $scoreboard = new Scoreboard();
        $scores = $scoreboard->getAllScores();
        require APP . 'view/_templates/header.php';
        require APP . 'view/scoreboard/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function addScore()
    {
        if($_POST['key'] === 'team-matioo') {
        $scoreboard = new Scoreboard();
        $player1 = $_POST["player1"];
        $player2 = $_POST["player2"];
        if ($scoreboard->userExists($player1)) {
            $scoreboard->updatePlayerScore($player1, 'win');
        } else {
            $scoreboard->addPlayerScore($player1, 'win');
        }
        if ($scoreboard->userExists($player2)) {
            $scoreboard->updatePlayerScore($player2, 'lose');
        } else {
            $scoreboard->addPlayerScore($player2, 'lose');
        }
    }
        header('location: ' . URL . 'game/duels');
    }
}