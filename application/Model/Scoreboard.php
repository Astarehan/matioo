<?php

/**
 * Class Scoreboard
 * This class is used to list the players and their score at Shrek Shots.
 */

namespace Mini\Model;

use Mini\Core\Model;

class Scoreboard extends Model
{
    public function getAllScores()
    {
        $request = 'SELECT * FROM scoreboard ORDER BY score DESC';
        $query = $this->db->prepare($request);
        $query->execute();
        return $query->fetchAll();
    }

    public function getPlayerScore($player)
    {
        $request = 'SELECT score FROM scoreboard WHERE player = :player';
        $query = $this->db->prepare($request);
        $params = array(':player' => $player);
        $query->execute($params);
        $obj = ($query->rowcount() ? $query->fetch() : false);
        return (int) $obj->score;
    }

    public function userExists($player)
    {
        $request = 'SELECT * FROM scoreboard WHERE player = :player';
        $query = $this->db->prepare($request);
        $params = array(':player' => $player);
        $query->execute($params);
        return $query->fetch();
    }

    public function addPlayerScore($player, $state)
    {
        $score = $state === 'win' ? 1 : 0;
        $request = 'INSERT INTO scoreboard (player, score) VALUES (:player, :score)';
        $query = $this->db->prepare($request);
        $params = array(
            ':player' => $player,
            ':score' => $score
        );
        $query->execute($params);
    }

    public function updatePlayerScore($player, $state)
    {
        $score = $this->getPlayerScore($player);
        if($state === 'win') {
            $score = $score + 1;
        } else {
            if ($score > 0) {
                $score = $score - 1;
            } else {
                $score = 0;
            }
        }
        $request = 'UPDATE scoreboard SET score = :score WHERE player = :player';
        $query = $this->db->prepare($request);
        $params = array(
            ':player' => $player,
            ':score' => $score
        );
        $query->execute($params);
    }
}