<?php
$winner = random_int(1, 3);
if ($winner === 3) {
    $result = 'draw';
    $winner = null;
    $loser = null;
} elseif ($winner === 2) {
    $result = '2';
    $winner = 'player-2';
    $loser = 'player-1';
} else {
    $result = '1';
    $winner = 'player-1';
    $loser = 'player-2';
}
?>
<div class="container" id="main-container">
    <div class="background shrek-bg" id="shrek-bg">
        <input id="player-1" type="text" class="input-shrek" placeholder="Joueur 1"/>
        <input id="buttonShrek" class="shrek-start" type="button" value="Start" />
        <input id="player-2" type="text" class="input-shrek" placeholder="Joueur 2"/>
        <input type="hidden" id="result" value="<?php echo $result ?>"/>
        <input type="hidden" id="winner" value="<?php echo $winner ?>"/>
        <input type="hidden" id="loser" value="<?php echo $loser ?>"/>
    </div>
    <div class="result-frame" id="result-frame"></div>
    <video src="../../res/<?php echo $result ?>.mp4" class="shrekvideo" id="shrekVideo" style="display: none"></video>
</div>
