<div class="container scoreboard-container">
    <div class="background scoreboard-bg" id="scoreboard-bg">
        <div class="scoreboard-title">Scoreboard</div>
        <!-- main content output -->
        <div class="box">
            <table class="box-table">
                <thead style="font-weight: bold;" class="box-thead">
                <tr>
                    <td>Player</td>
                    <td>Score</td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($scores as $score) { ?>
                    <tr>
                        <td><?php if (isset($score->player)) echo htmlspecialchars($score->player, ENT_QUOTES, 'UTF-8'); ?></td>
                        <td><?php if (isset($score->score)) echo htmlspecialchars($score->score, ENT_QUOTES, 'UTF-8'); ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
